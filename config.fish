if status is-interactive
    # Commands to run in interactive sessions can go here
    zoxide init fish | source
    abbr --add ls eza
    abbr --add ll eza -l --git --header
    abbr --add cat bat
    abbr --add cr cargo run
    abbr --add ct cargo test
    abbr --add cd z
    abbr --add tsp ts start -t planning -d \"Project and task management\"
    abbr --add tsy ts summary --yesterday -V
    abbr --add tsw ts summary -wV
    abbr --add ssh-agent-add 'eval (ssh-agent -c) && ssh-add'
    fish_add_path ~/coding/projects/ts-cli/target/release
    fish_add_path ~/.cargo/bin
    set -gx EDITOR hx
end

# Added by Radicle.
export PATH="$PATH:/home/kris/.radicle/bin"
