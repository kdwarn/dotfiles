# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
  # include .bashrc if it exists
  if [ -f "$HOME/.bashrc" ]; then
    . "$HOME/.bashrc"
  fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
  PATH="$HOME/bin:$PATH"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
  PATH="$HOME/.local/bin:$PATH"
fi

# load cargo's environment variables
. "$HOME/.cargo/env"

# environment variables
export DOTNET_CLI_TELEMETRY_OPTOUT=1  # turn off dotnet telemetry
export EDITOR="hx"
export zk="$HOME/zk"
export NVM_DIR="$HOME/.nvm"
# for borg backups to rsync.net
export BORG_REMOTE_PATH="/usr/local/bin/borg1/borg1"

# additional path settings
# export PATH="$PATH:$HOME/coding/projects/ts-cli/target/release"

# run anacron on start up
anacron -t "$HOME/.anacron/etc/anacrontab" -S "$HOME/.anacron/var/spool/anacron"
