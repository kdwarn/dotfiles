# README

Here's a blog post describing how this works: [Syncing dotfiles](https://kdwarn.dev/blog/syncing-dotfiles).

I'll probably use this most for copying files over to remote servers, so here's a reminder on how to do that:
  1. git the "raw file" link: navigate to a file, then click on the "raw" button at the top right
  2. `wget url_to_that_file`, e.g. `wget https://codeberg.org/kdwarn/dotfiles/raw/branch/main/.vimrc`
