" colorize 
syn match   pythonComment	"#.*$" contains=pythonTodo,@Spell
syn keyword pythonTodo		FIXME NEXT NOTE NOTES TODO XXX contained
