" this goes in the ~/.vim/after/syntax folder, and will apply some custom
" formatting to markdown files

" using @something for tagging, so make them display in different color 
syntax match markdownTag "\v\@[a-z0-9]+"
highlight link markdownTag Statement

" don't color like a tag if @ follows word character or digit
syntax match notMarkdownTag "\v(\w|\d)\@[a-z0-9]+"
highlight link notMarkdownTag Normal

" colorize [[things in double brackets]]
syntax match markdownRef "\v\[\[[-a-z0-9\.\/\:]+\]\]"
" highlight link markdownRef Type
highlight link markdownRef Identifier

" colorize TODO:, NOTE:, and NEXT (and lowercase versions)
syntax match markdownTODO "\v(TODO:|NOTE:|NEXT:)"
" highlight link markdownTODO Todo
hi link markdownTODO MatchParen

" colorize detour(s): (and uppercase versions):
syntax match markdownDetour "\v(detours:|detour:)"
hi link markdownDetour Operator

" colorize papercuts, question, questions, warning
syntax match markdownPapercuts "\v(papercuts:|question:|questions:|warning:)"
hi link markdownPapercuts GruvboxYellow

" highlight the text between backticks, including the ticks
syntax match singleLineFence "\v`[^`]+`"
highlight link singleLineFence GruvboxAquaSign

" all levels of markdown lists treated as markdown lists rather that fenced code
" from https://github.com/tpope/vim-markdown/issues/4#issuecomment-128466977
syntax match markdownListMarker 
     \ "\%(\t\| \+\)[-*+]\%(\s\+\S\)\@=" contained
syntax match markdownOrderedListMarker
     \ "\%(\t\| \+\)\<\d\+\.\%(\s\+\S\)\@=" contained

" don't include _ as markdown error
" <https://stackoverflow.com/questions/19137601/turn-off-highlighting-a-certain-pattern-in-vim>
syn match markdownError "\w\@<=\w\@="

" ignore markdown error highlighting
" highlight link markdownError Normal

