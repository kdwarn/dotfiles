" All system-wide defaults are set in $VIMRUNTIME/debian.vim and sourced by
" the call to :runtime you can find below.  If you wish to change any of those
" settings, you should do it in this file (/etc/vim/vimrc), since debian.vim
" will be overwritten everytime an upgrade of the vim packages is performed.
" It is recommended to make changes after sourcing debian.vim since it alters
" the value of the 'compatible' option.

" This line should not be removed as it ensures that various options are
" properly set to work with the Vim-related packages available in Debian.
" runtime! debian.vim

" Vim will load $VIMRUNTIME/defaults.vim if the user does not have a vimrc.
" This happens after /etc/vim/vimrc(.local) are loaded, so it will override
" any settings in these files.
" If you don't want that to happen, uncomment the below line to prevent
" defaults.vim from being loaded.
" let g:skip_defaults_vim = 1

" Uncomment the next line to make Vim more Vi-compatible
" NOTE: debian.vim sets 'nocompatible'.  Setting 'compatible' changes numerous
" options, so any other options should be set AFTER setting 'compatible'.
"set compatible
"
" KW: Generally, all neovim stuff is from 
" <https://sharksforarms.dev/posts/neovim-rust/>
" though my config will likely to start to diverage. This was at least the
" starting point for setting up neovim, and specifically for Rust dev.

" Source a global configuration file if available
if filereadable("/etc/vim/vimrc.local")
  source /etc/vim/vimrc.local
endif

" Uncomment the following to have Vim jump to the last position when
" reopening a file
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

" Install vim-plug (<https://github.com/junegunn/vim-plug>):
" curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
" Then to install plugins: 
" add a new Plug entry, reload .vimrc, and call :PlugInstall
call plug#begin('~/.vim/plugged')
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'preservim/tagbar'
Plug 'dense-analysis/ale'
Plug 'SirVer/ultisnips'
Plug 'vim-python/python-syntax'
Plug 'pangloss/vim-javascript'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-unimpaired'
Plug 'rhysd/vim-grammarous'
Plug 'rust-lang/rust.vim' 
" <git-related plugins>
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'
Plug 'junegunn/gv.vim'
" </git-related plugins>
Plug 'tyru/open-browser.vim'
Plug 'gruvbox-community/gruvbox'
Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'
Plug 'Yggdroot/indentLine'
Plug 'dhruvasagar/vim-table-mode'

if has('nvim')
    Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
    " everything below is from <https://sharksforarms.dev/posts/neovim-rust/>
    Plug 'neovim/nvim-lspconfig' " Collection of common configurations for the Nvim LSP client
    Plug 'hrsh7th/nvim-cmp' " Completion framework
    Plug 'hrsh7th/cmp-nvim-lsp' " LSP completion source for nvim-cmp
    " Plug 'hrsh7th/cmp-vsnip' " Snippet completion source for nvim-cmp
    " Other usefull completion sources:
    Plug 'hrsh7th/cmp-path' 
    "Plug 'hrsh7th/cmp-buffer'
    " See hrsh7th's other plugins for more completion sources!
    Plug 'simrat39/rust-tools.nvim' " To enable more of the features of rust-analyzer, such as inlay hints and more!
    Plug 'hrsh7th/vim-vsnip' " Snippet engine

    " Fuzzy finder
    Plug 'nvim-lua/popup.nvim'
    Plug 'nvim-lua/plenary.nvim'
    Plug 'nvim-telescope/telescope.nvim'

endif
call plug#end()

" Uncomment the following to have Vim load indentation rules and plugins
" according to the detected filetype.
filetype plugin indent on

set showcmd " Show (partial) command in status line.
set showmatch " Show matching brackets.
set ignorecase " Do case insensitive matching
set smartcase " Do smart case matching
set incsearch " Incremental search
set autowrite " Automatically save before commands like :next and :make
set hidden " Hide buffers when they are abandoned
set mouse=a " Enable mouse usage (all modes)
set number  " row line numbers
set ruler  " ruler at botom of screen that shows line number, % through file
set hlsearch  " highlight all instances of search term
set wrap
set linebreak  " when soft wrapping, break after word, not in middle of it
set breakindent " when soft wrapping, keep indent level the same
set colorcolumn=100 " set a vertical ruler
set cursorline  " highlight current line

" tabbing
set tabstop=4
set softtabstop=4
set expandtab
set shiftwidth=4
set smarttab
set autoindent

" persistent undo set undofile                
if has("nvim")
    set undodir=$HOME/.config/nvim/undo
else
    set undodir=$HOME/.vim/undo
endif
set undofile
" set undodir=$HOME/.vim/undo " where to save undo histories
set undolevels=100          " How many undos
set undoreload=1000         " number of lines to save for undo

set wildignore=*/ve/*,*/__pycache__/*,*/htmlcov/*,*/dist*
set nofoldenable  " make folds open by default

" personal spelling dictionary
set spellfile=~/.vim/spell/en.utf-8.add 

" vim doesn't recognize .md files as markdown, but as something else, so change that
autocmd BufNewFile,BufFilePre,BufRead *.md set filetype=markdown
autocmd BufNewFile,BufFilePre,BufRead *.yaml set filetype=yaml
autocmd BufNewFile,BufFilePre,BufRead *.j2 set filetype=htmldjango
autocmd BufNewFile,BufFilePre,BufRead *.jrnl set filetype=jrnl
autocmd BufNewFile,BufFilePre,BufRead *.rkt,*.rktl,*.scm set filetype=scheme

augroup my_filetypes
    autocmd!
    autocmd FileType markdown setlocal colorcolumn=0 ts=2 sts=2 sw=2 spell
    autocmd FileType text setlocal colorcolumn=0 spell
    autocmd FileType html setlocal ts=2 sts=2 sw=2
    autocmd FileType css setlocal ts=2 sts=2 sw=2
    autocmd FileType less setlocal ts=2 sts=2 sw=2
    autocmd FileType htmldjango setlocal ts=2 sts=2 sw=2
    autocmd FileType yaml setlocal ts=2 sts=2 sw=2 foldmethod=indent
    autocmd FileType json setlocal ts=2 sts=2 sw=2
    autocmd FileType javascript setlocal ts=2 sts=2 sw=2
    autocmd FileType sh setlocal ts=2 sts=2 sw=2
    autocmd FileType python setlocal foldmethod=indent
    autocmd Filetype gitcommit setlocal spell tw=72 cc=73
    autocmd Filetype jrnl setlocal ts=2 sts=2 sw=2 spell
augroup END
" end various settings for specific file types

" prevent terminals from being included in buffer list/navigation
" <https://vi.stackexchange.com/a/16709/29304>
if !has('nvim')
    augroup termIgnore
        autocmd!
        autocmd TerminalOpen * set nobuflisted
    augroup END
endif


" Vim5 and later versions support syntax highlighting. Uncommenting the next
" line enables syntax highlighting by default.
syntax on

let g:is_bash = 1

let g:python_highlight_all = 1

" syntax highlighting within markdown
" look in /usr/share/vim/vim82/syntax folder for names - took me a minute to
" figure out that C# is referred to as cs
let g:markdown_fenced_languages = [
             \ 'asm',
             \ 'bash=sh', 
             \ 'c', 
             \ 'cs', 
             \ 'css', 
             \ 'elm',
             \ 'gitconfig', 
             \ 'html',
             \ 'javascript', 
             \ 'js=javascript', 
             \ 'nginx',
             \ 'python', 
             \ 'rust',
             \ 'sh', 
             \ 'sql', 
             \ 'toml',
             \ 'vim',
             \ 'yaml'
             \]
let g:markdown_syntax_conceal = 0

set conceallevel=0

let g:markdown_folding = 1

" open files in netrw window in previous window
let g:netrw_browse_split = 4


" colorscheme; dark/light

" This fixes the background color erasing that I was experiencing in WSL at work 
" Found here: https://github.com/microsoft/terminal/issues/832#issuecomment-502454504
" Also experience this using straight Linux (Pop_OS) and using vim from inside
" Zellij. Using `set t_ut=""` stops it from happening.
" if (&term =~ '^xterm' && &t_Co == 256)
"   set t_ut= | set ttyscroll=1
" endif
"
" or just: 
set t_ut=""

function ToggleBackground()
    if (&background ==# "dark")
        set background=light
        let @b = 'light'
    else
        set background=dark
        let @b = 'dark'
    endif
endfunction

nnoremap <silent> <Leader>b :call ToggleBackground()<CR>

let g:gruvbox_guisp_fallback = "bg"
let g:gruvbox_italic=1
let g:gruvbox_contrast_dark='soft'
let g:gruvbox_contrast_light='soft'

" b/c registers are stored in .viminfo, and that's loaded after this file,
" load it now
rviminfo
let &bg = @b

set termguicolors

colorscheme gruvbox

" vim-airline config
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#buffer_nr_show = 1
let g:airline#extensions#tagbar#enabled = 1
let g:airline#extensions#tagbar#flags = 'f'
let g:airline#extensions#tagbar#searchmethod = 'scoped-stl'
let g:airline_theme='gruvbox'

" Ale config
noremap K :ALEHover<CR>
set omnifunc=ale#completion#OmniFunc
let g:ale_fix_on_save = 1
let g:ale_floating_preview = 0  " this had been 1, but I think i preview a regular window
let g:ale_echo_msg_error_str = 'E'
let g:ale_echo_msg_warning_str = 'W'
let g:ale_echo_msg_format = '[%linter%] [%severity%] %code%: %s'

let g:rust_recommended_style = 1
let g:rust_fold = 1
let g:rustfmt_autosave = 1

let g:ale_python_black_options = '--line-length=100'
let g:ale_python_pylsp_config = {
\     'pylsp': {
\         'plugins': {
\             'pycodestyle': {
\                 'ignore': ['E501'],
\             }
\         }
\     }
\ }
let g:ale_python_mypy_options = '--ignore-missing-imports'

let g:ale_linters = {
    \ 'javascript': ['jshint'],
    \ 'python': ['pylsp', 'mypy'],
    \ 'yaml': ['yamllint'],
    \ }

" set up rust linting differently depending on vim/neovim
" use rust-analyzer via ALE for vim
if !has('nvim')
    let g:ale_linters.rust = ['analyzer']
endif
" no linting via ALE for neovim (configured separately in neovim's init.vim via built-in lsp)
if has('nvim')
    let g:ale_linters.rust = []
endif

let g:ale_fixers = {
    \ 'markdown': ['trim_whitespace', 'remove_trailing_lines'],
    \ 'python': ['black', 'trim_whitespace', 'remove_trailing_lines'],
    \ 'javascript': ['prettier'],
    \ 'bash': ['trim_whitespace', 'remove_trailing_lines'],
    \ 'yaml': ['trim_whitespace', 'remove_trailing_lines'],
    \ }

" UltiSnips config
let g:UltiSnipsSnippetDirectories=["UltiSnips", "kw_ultisnips"]
let g:UltiSnipsExpandTrigger = '<c-j>'
let g:UltiSnipsJumpForwardTrigger = '<c-j>'
let g:UltiSnipsJumpBackwardTrigger = '<c-k>'

" Grammarous config
let g:grammarous#disabled_rules = {
    \ '*' : ['COMMA_COMPOUND_SENTENCE']
    \ }

" shortcuts for Grammarous https://github.com/rhysd/vim-grammarous/issues/34#issuecomment-282925612
let g:grammarous#hooks = {}
function! g:grammarous#hooks.on_check(errs) abort
    nmap <buffer>]g <Plug>(grammarous-move-to-next-error)
    nmap <buffer>[g <Plug>(grammarous-move-to-previous-error)
endfunction
function! g:grammarous#hooks.on_reset(errs) abort
    nunmap <buffer>]g
    nunmap <buffer>[g
endfunction

" debug
let g:termdebugger="rust-gdb"

" some custom shortcuts
noremap <Leader>y "+y
nnoremap <Leader>p "+p
nnoremap <Leader>o :lopen<CR>
noremap gz :!zkgrep <cWORD><CR><CR>
nnoremap <Leader>v :vs<CR>gf<CR>
nnoremap <Leader>h :split<CR>gf<CR>
" copy relative filepath, surrounded by [[]], & 1st line of file into unnamed register
nnoremap <silent> <Leader>l :let @y = "[[" . expand("%:.") . "]] " <bar> :1,1y z <bar> let @" = @y . @z<CR>
nnoremap <Leader>g 0gf
" map jk in insert mode to Esc
inoremap jk <Esc>
" create three windows, one of them a small terminal ("nd" for 'new (vim) desktop')
noremap <silent> <Leader>nd :vsplit \| :winc l \| :exec "below term" \| :15winc - \| :winc h<CR>
" close current buffer - but first switch to previous buffer so window doesn't get closed
noremap <Leader>c :bp\|:bd#<CR>
" open file in previous window, from
" <https://unix.stackexchange.com/questions/74571/vim-shortcut-to-open-a-file-under-cursor-in-an-already-opened-window>
nnoremap <silent> gp :let mycurf=expand("<cfile>")<cr><c-w>p:execute("e ".mycurf)<cr>
" use tab/shift tab for :bn/:bp
nnoremap <silent> <tab> :bn<CR>
nnoremap <silent> <S-tab> :bp<CR>
" quicker save
nmap <silent> <Leader>w :w<CR>
" get to Terminal-Normal mode quicker
tnoremap <C-n> <C-w>N

" pull up zk lines with @reading or @next in them
nnoremap <Leader>r :vimgrep/@reading/ **/*<CR> \| :copen<CR>
nnoremap <Leader>z :vimgrep/@zknext/ **/*<CR> \| :copen<CR>

" function to save file with timestamp as filename, it not already saved
function! SaveWithTS()
    if !expand('%:t')
        let l:filename = escape( strftime("%Y-%m-%d-%H%M"), ' ' )
        execute "write " . "~/zk/notes/" . l:filename . ".md"
    else
        echo "File already saved and named."
    endif
endfunction

command! Zk call SaveWithTS()

" Fix netrw issue https://github.com/vim/vim/issues/4738
if !has('nvim')
    let g:netrw_nogx = 1
    nmap gx <Plug>(openbrowser-smart-search)
    vmap gx <Plug>(openbrowser-smart-search)
endif


