alias grep="grep --color"  # always colorize the search term in the output
alias bc="bc -l"
alias v=nvim
alias f=flashcards
alias zk="cd $zk && hx index.md"
alias cr="cargo run"
alias ct="cargo test"
